﻿using HelloWCF.Controllers;
using HelloWCF.Model;
using HelloWCF.Model.Interfaces;
using System.ServiceModel;

namespace HelloWCF
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MatrixService : IMatrixService
    {
        private MatrixController _matrixController = new MatrixController();

        /// <summary>
        /// Multiplies two matrixes present in service. On success returns index of new matrix, else -1.
        /// </summary>
        /// <param name="idA">Index of matrix A</param>
        /// <param name="idB">Index of matrix B</param> 
        public int Multiply(int idA, int idB)
        {
            return _matrixController.Multiply(idA, idB);
        }

        /// <summary>
        /// Inserts matrix to service based on received array. Index of matrix in system on success, else -1.
        /// </summary>
        /// <param name="MatrixProxy">Object filled with data: id, packetId, Data, RowsCount, ColumnsCount</param>        
        public int Upload(MatrixProxy proxy)
        {                       
            return _matrixController.InsertMatrix(proxy);            
        }

        /// <summary>
        /// Removes matrix of given index in service. 0 on success, else -1
        /// </summary>
        /// <param name="index">Index of matrix to be removed</param>   
        public bool Remove(int id)
        {
            if (_matrixController.RemoveMatrix(id) == 0)
                return true;
            return false;
        }

        /// <summary>
        /// Returns Matrix stored in service. Matrix on succes, else null.
        /// </summary>
        /// <param name="id">Index of matrix</param>        
        /// <param name="packetId">Which packet of matrix (sizeof <MaxPacketSize>)</param>     
        public MatrixProxy GetMatrixProxy(int id, int packetId)
        {
            return _matrixController.GetProxy(id, packetId);            
        }

        /// <summary>
        /// Returns available indexes of matrixes in service. Array of int on succes, else null.
        /// </summary>
        /// <returns></returns>
        public int[] GetAvailableMatrixes()
        {
            return _matrixController.GetAvailableIndexes();
        }

        /// <summary>
        /// Returns MaxPacketSize
        /// </summary>
        /// <returns></returns>
        public int GetMaxPacketSize()
        {
            return MatrixController.MaxPacketSize;
        }        
    }
}
