﻿using HelloWCF.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HelloWCF.Controllers
{
    public class MatrixController
    {
        public static int MaxPacketSize { get; } = 100;
        private List<Matrix> _matrixes = new List<Matrix>();

        /// <summary>
        /// Multiplies two matrixes present in service. On success returns index of new matrix, else -1.
        /// </summary>
        /// <param name="idA">Index of matrix A</param>
        /// <param name="idB">Index of matrix B</param>        
        public int Multiply(int idA, int idB)
        {
            var newMatrix = new Matrix(_matrixes[idA].RowsCount, _matrixes[idB].ColumnsCount);

            if (_matrixes[idA].ColumnsCount == _matrixes[idB].RowsCount)
            {
                int wynik = 1;
                var watch = System.Diagnostics.Stopwatch.StartNew();

                Parallel.For(0, _matrixes[idA].RowsCount, a => {
                    wynik = 0;
                    for (int b = 0; b < _matrixes[idB].ColumnsCount; ++b)
                    {
                        wynik = 0;
                        for (int c = 0; c < _matrixes[idA].ColumnsCount; ++c)
                        {
                            wynik += _matrixes[idA].Values[a, c] * _matrixes[idB].Values[c, b];
                        }

                        newMatrix.Values[a, b] = wynik;
                    }
                });                
                watch.Stop();
                newMatrix.CalculationTime = (int)watch.ElapsedMilliseconds;
                _matrixes.Add(newMatrix);
                return _matrixes.IndexOf(newMatrix);
            }

            return -1;            
        }
        
        /// <summary>
        /// Returns Matrix stored in service. Matrix on succes, else null.
        /// </summary>
        /// <param name="index">Index of matrix</param>        
        public MatrixProxy GetProxy(int index, int packetId)
        {            
            if (index < _matrixes.Count)
            {
                var packets = (_matrixes[index].ColumnsCount * _matrixes[index].RowsCount) / MaxPacketSize;

                if (packetId>0 && packets <= packetId)
                    return null;

                var dataPacket = new int[_matrixes[index].Size()-(packetId*MaxPacketSize)<MaxPacketSize ? _matrixes[index].Size() : MaxPacketSize];
                var mainProxy = _matrixes[index].ToProxy();

                for (int j = 0, i = MaxPacketSize * packetId;
                        j < dataPacket.Length;
                        ++i, ++j)
                    dataPacket[j] = mainProxy.Data[i];

                var proxy = new MatrixProxy(dataPacket, MaxPacketSize, _matrixes[index].RowsCount, _matrixes[index].ColumnsCount);
                proxy.PacketId = packetId;

                return proxy;
            }
                
            return null;
        }

        /// <summary>
        /// Inserts matrix to service based on received array. Index of matrix in system on success, else -1.
        /// </summary>
        /// <param name="array">Array containing data</param>
        /// <param name="lineLength">Columns of matrix</param>        
        public int InsertMatrix(MatrixProxy proxy)
        {
            if (proxy.Id == -1)
            {
                var matrix = new Matrix(proxy);
                _matrixes.Add(matrix);
                return _matrixes.IndexOf(matrix);
            }
            else if (proxy.Id < _matrixes.Count)
            {
                if (_matrixes[proxy.Id].InsertData(proxy))
                    return proxy.Id;
                return -1;
            }
            else return -1;
            
        }

        /// <summary>
        /// Removes matrix of given index in service. 0 on success, else -1
        /// </summary>
        /// <param name="index">Index of matrix to be removed</param>       
        public int RemoveMatrix(int index)
        {
            if (_matrixes.Remove(_matrixes[index]))
                return 0;
            return -1;
        }

        /// <summary>
        /// Returns available indexes of matrixes in service. Array of int on succes, else null.
        /// </summary>
        /// <returns></returns>
        public int[] GetAvailableIndexes()
        {
            if (_matrixes.Count < 1)
                return null;

            var array = new int[_matrixes.Count];
            for (int i=0; i<array.Length; ++i)
            {
                array[i] = i;
            }
            return array;
        }       
    }
}