﻿using System.ServiceModel;

namespace HelloWCF.Model.Interfaces
{
    [ServiceContract]
    public interface IMatrixService
    {       
        [OperationContract]
        int Multiply(int idA, int idB);

        [OperationContract]
        int Upload(MatrixProxy proxy);

        [OperationContract]
        bool Remove(int id);

        [OperationContract]
        MatrixProxy GetMatrixProxy(int id, int packetId);

        [OperationContract]
        int[] GetAvailableMatrixes();        

        [OperationContract]
        int GetMaxPacketSize();
    }
}
