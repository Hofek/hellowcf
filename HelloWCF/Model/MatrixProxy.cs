﻿using System;
using System.Runtime.Serialization;

namespace HelloWCF.Model
{
    [DataContract]
    public class MatrixProxy
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int[] Data { get; set; }
        [DataMember]
        public int PacketId { get; set; }
        [DataMember]
        public int PacketSize { get; set; }
        [DataMember]
        public int RowsCount { get; set; }
        [DataMember]
        public int ColumnsCount { get; set; }       
        [DataMember]
        public string CalculationTime { get; set; }
      
        public MatrixProxy(int[] array, int packetSize, int rowsCount, int columnsCount)
        {
            Data = array;
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
        }

        public int MatrixSize()
        {
            return RowsCount * ColumnsCount;
        }
    }
}