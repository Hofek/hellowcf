﻿using HelloWCF.Controllers;
namespace HelloWCF.Model
{    
    public class Matrix
    {           
        public int[,] Values { get; set; }       
        public int RowsCount { get; set; }       
        public int ColumnsCount { get; set; }      
        public int CalculationTime { get; set; }

        /// <summary>
        /// Creates instance of matrix based on provided proxy
        /// </summary>
        /// <param name="proxy"></param>
        public Matrix(MatrixProxy proxy)
        {
            RowsCount = proxy.RowsCount;
            ColumnsCount = proxy.ColumnsCount;            
            Values = new int[RowsCount,ColumnsCount];
            int i = 0, j = 0, k=0;

            for (; i < RowsCount && k < proxy.Data.Length; ++i)
                for (j = 0; j < ColumnsCount && k < proxy.Data.Length; ++j, ++k)
                    Values[i, j] = proxy.Data[k];
        }

        /// <summary>
        /// Creates empty instance of matrix
        /// </summary>
        /// <param name="columnsCount"></param>
        /// <param name="rowsCount"></param>
        public Matrix(int rowsCount, int columnsCount)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            Values = new int[rowsCount, columnsCount];            
        }

        /// <summary>
        /// Casts matrix to proxy object.
        /// </summary>
        /// <returns></returns>
        public MatrixProxy ToProxy()
        {            
            int[] array = new int[RowsCount * ColumnsCount];

            for (int i = 0; i < RowsCount; ++i)
                for (int j = 0; j < ColumnsCount; ++j)
                    array[(i * ColumnsCount) + j] = Values[i, j];

            return new MatrixProxy(array, ColumnsCount*RowsCount, RowsCount, ColumnsCount);
        }

        public bool InsertData(MatrixProxy proxy)
        {
            if (proxy.ColumnsCount != ColumnsCount || proxy.RowsCount != RowsCount)
                return false;

            if ((RowsCount * ColumnsCount) / MatrixController.MaxPacketSize < proxy.PacketId)
                return false;

            int i = 0, j = 0, k=0;

            //calculate values of indexes based on packetId
            while (i * ColumnsCount < MatrixController.MaxPacketSize * proxy.PacketId)
                ++i;
            if (i*ColumnsCount > MatrixController.MaxPacketSize*proxy.PacketId)
                --i;
            while (i*ColumnsCount + j < MatrixController.MaxPacketSize * proxy.PacketId)
                ++j;
            //

            for (; i < RowsCount && k<proxy.Data.Length; ++i)
            {
                for (; j < ColumnsCount && k < proxy.Data.Length; ++j, ++k)                
                    Values[i, j] = proxy.Data[k];                    
                                        
                j = 0;
            }
                
                    

            return true;
        }

        public int Size()
        {
            return ColumnsCount * RowsCount;
        }
    }
}