﻿using HelloWCFClient.HelloWCF;
using System;

namespace HelloWCFClient.Model
{
    public class Matrix
    {
        public int[,] Values { get; set; }
        public int RowsCount { get; set; }
        public int ColumnsCount { get; set; }
        public int CalculationTime { get; set; }

        /// <summary>
        /// Creates instance of matrix based on provided proxy
        /// </summary>
        /// <param name="proxy"></param>
        public Matrix(MatrixProxy proxy)
        {
            RowsCount = proxy.RowsCount;
            ColumnsCount = proxy.ColumnsCount;

            if (Int32.TryParse(proxy.CalculationTime, out var time))
                CalculationTime = time;

            Values = new int[RowsCount, ColumnsCount];
            int i = 0, j = 0, k = 0;

            for (; i < RowsCount && k < proxy.Data.Length; ++i)
                for (j = 0; j < ColumnsCount && k < proxy.Data.Length; ++j, ++k)
                    Values[i, j] = proxy.Data[k];
        }

        /// <summary>
        /// Creates empty instance of matrix
        /// </summary>
        /// <param name="columnsCount"></param>
        /// <param name="rowsCount"></param>
        public Matrix(int rowsCount, int columnsCount)
        {
            RowsCount = rowsCount;
            ColumnsCount = columnsCount;
            Values = new int[rowsCount, columnsCount];
        }

        /// <summary>
        /// Randomize values in matrix
        /// </summary>
        /// <param name="min">Minimum value of randomize</param>
        /// <param name="max">Maximum value of randomize</param>
        public void Rand(int min, int max)
        {
            Random random = new Random();

            for (int i = 0; i < RowsCount; ++i)
                for (int j = 0; j < ColumnsCount; ++j)
                    Values[i, j] = random.Next(min, max);
        }

        /// <summary>
        /// Casts matrix to proxy object.
        /// </summary>
        /// <returns></returns>
        public MatrixProxy ToProxy()
        {
            int[] array = new int[RowsCount * ColumnsCount];

            for (int i = 0; i < RowsCount; ++i)
                for (int j = 0; j < ColumnsCount; ++j)
                    array[(i * ColumnsCount) + j] = Values[i, j];

            return new MatrixProxy(array, Program.Client.GetMaxPacketSize(), RowsCount, ColumnsCount);
        }

        public void Print()
        {
            for (int i = 0; i < RowsCount; ++i)
            {
                for (int j = 0; j < ColumnsCount; ++j)
                    Console.Write(Values[i, j] + " ");
                Console.WriteLine();
            }
            Console.WriteLine($"Czas liczenia: {CalculationTime}ms");
        }

        public bool InsertData(MatrixProxy proxy)
        {
            if (proxy.ColumnsCount != ColumnsCount || proxy.RowsCount != RowsCount)
                return false;

            var maxPacketSize = Program.Client.GetMaxPacketSize();

            if ((RowsCount * ColumnsCount) / maxPacketSize < proxy.PacketId)
                return false;
            
            int i = 0, j = 0, k = 0;

            //calculate values of indexes based on packetId
            while (i * ColumnsCount < maxPacketSize * proxy.PacketId)
                ++i;
            if (i * ColumnsCount > maxPacketSize * proxy.PacketId)
                --i;
            while (i * ColumnsCount + j < maxPacketSize * proxy.PacketId)
                ++j;
            //

            for (; i < RowsCount && k < proxy.Data.Length; ++i)
            {
                for (; j < ColumnsCount && k < proxy.Data.Length; ++j, ++k)
                    Values[i, j] = proxy.Data[k];

                j = 0;
            }

            return true;
        }
    }
}
